def paretoFront(list1):
    mark_list= []
    pareto1 = []
    # Making empty mark_list
    for i in range(len(list1)):
        message = 0
        mark_list.append(message)

    #sort list accending order
    list1 =  sorted(list1, key=lambda x: x.fitness.values[0])

    #Mark list at points where feature[0] changes
    old_value = list1[0].fitness.values[0]
    for index , ind in enumerate(list1):
        if(list1[index].fitness.values[0] == old_value):
            None
        else:
            mark_list[index] = 1
        old_value = list1[index].fitness.values[0];

    #Use the change points made by mark_list to find the best time in each set
    bestForFeatO =  []
    tempbest = list1[0]
    mark_list.append(1)
    for index in range(len(list1)):
        if (mark_list[index] == 1):
            tempbest = list1[index];
        if(index < len(mark_list)-1  and  mark_list[index+1] == 1):
            bestForFeatO.append(tempbest)
        if( list1[index].fitness.values[1] < tempbest.fitness.values[1]):
            tempbest =  list1[index];

    #Print
    #for index , ind in enumerate(bestForFeatO):
        #print bestForFeatO[index].fitness.values

    #Grab the final pareto points
    temp_time = bestForFeatO[0].fitness.values[1]
    pareto1.append(bestForFeatO[0])
    for index , ind in enumerate(bestForFeatO):
        if(bestForFeatO[index].fitness.values[1] < temp_time ):
            temp_time = bestForFeatO[index].fitness.values[1]
            pareto1.append(bestForFeatO[index])

    # make the nonPareto points
    nonPareto = list1[:]
    for index1 , ind1 in enumerate(pareto1):
        for index2 , ind2 in enumerate(nonPareto):
            if (ind1 == ind2  and ind1.fitness.values ==  ind2.fitness.values):
                del nonPareto[index2]
    return pareto1, nonPareto

def paretoBand (list1, numberoffronts):
    pareto1, nonPareto1  = paretoFront(list1)
    pareto2, nonPareto2  = paretoFront(nonPareto1)
    combined2 = pareto2 + pareto1

    if numberoffronts == 1:
        return pareto1

    if numberoffronts == 2:
        return combined2;

    ## TO DO WRITE for N Paretofronts
    #nonParetoNMinus1 = list1
    #for i in range numberoffronts:
    #paretoN, nonParetoN  = paretoFront(nonParetoNMinus1)




    #for index , ind in enumerate(pareto1):
        #print pareto1[index].fitness.values
    #print "---------------------------------"
    #for index , ind in enumerate(nonPareto1):
        #print nonPareto1[index].fitness.values
    #print "======================="
    #print "======================="
    #for index , ind in enumerate(pareto2):
        #print pareto2[index].fitness.values
    #print "---------------------------------"
    #for index , ind in enumerate(nonPareto2):
        #print nonPareto2[index].fitness.values












from genSpec import *
import os

##folder variables
COUNTER = 0
DESIGNSPACE = 'designSpace/'
SPECSIRFILE = 'susan_edge_detector.sir'
CHEATCODES = True #enable hacks

#to keep track of what is allocated to what
class PE:
	def __init__(self):
		self.type = "none" #arm or hardware
		self.behaviors = []
		self.name = "none"

#declare the buckets to save info in

def getNumThingsToAllocate(encoding):
	sumAss= encoding[1][0]
	sumAss+= encoding[1][1]
	sumAss+= encoding[1][2]
	if sumAss == 0:
		return 3
	if sumAss == 1:
		return 3
	if sumAss == 2:
		return 2
	if sumAss == 3:
		return 1


#test encoding
#encoding = [[1,0,1],[1,0,1]]
#encoding = [[1,1,1],[1,1,1]]
#encoding = [[1,0,1],[0,0,0]]
#encoding = [[1,0,0],[1,0,0]]

BehaviorNames = ['edges','thin','draw']
ARMNames = ['ARM0', 'ARM1', 'ARM2']
HWNames = ['HW0', 'HW1', 'HW2']
TypeMap = ['HW_Standard', 'ARM_7TDMI']
COST = [150, 100]
NametoTypeMap = {'HW_Standard' : 0, 'ARM_7TDMI' : 1}
NumPEs = [0,0] # size of this array = number of different PE types
							# means [# HWs allocated, # of ARMS allocated, etc]
NameArrs = [HWNames, ARMNames]
#PElist = [PE0, PE1, PE2]
PE0 = PE()
PE1 = PE()
PE2 = PE()

PElist = [PE0, PE1, PE2]

PRIOS = 'ARM_7TDMI_OS_PRIORITY_20000_0'

def evalSpecFitness(encoding): ##call this functiont:
	#print "sumAssociation = ", sumAssociation
	#list of my PEs
	global PE0 
	global PE1 
	global PE2 
	global COUNTER
	COUNTER += 1
	
	os.chdir('/scratch/mlin1/gerstlauer/geneticAlg/referencework')
	if CHEATCODES == True: #enable hacks
		if os.path.exists(DESIGNSPACE + EncodingToString(encoding)):
			os.chdir(DESIGNSPACE + EncodingToString(encoding))		#cd there
			f = open("results.txt", 'r')
			line = f.readline()
			linesplit = line.split(" ")
			return int(linesplit[0]), int(linesplit[1])


	PE0 = PE()
	PE1 = PE()
	PE2 = PE()
	global PElist
	PElist = [PE0, PE1, PE2]
	NumPEs[0] = 0
	NumPEs[1] = 0
	numThings = getNumThingsToAllocate(encoding)
	MapBehaviorsToPEs(numThings, encoding)	
## end preprocessings, start scripting here
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	#os.system('./cleaner.sh') # removes everything in the design space folder
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	######################## REMOVE THIS ######################################
	encStr = EncodingToString(encoding)   
	CMD = 'mkdir '+ DESIGNSPACE + encStr  # create folder in designSpace/encodingName
	os.system(CMD)
	os.chdir(DESIGNSPACE + encStr)		#cd there
	os.system('cp ../../'+ SPECSIRFILE + ' .') # cp the golder sir file
	os.system('cp ../../input_small.pgm .')
	os.system('cp ../../golden.pgm .')
	print	NametoTypeMap['HW_Standard']

#start sce scripting
	DesignName = 'susan_edge_detector'
	InsProf(DesignName) #instrument for profiling
						#compile for execution
	CompileForExecutionNP(DesignName)
	Run(DesignName) #maybe add a check in this function
	#x = raw_input('please enter a string:')
	CompileForExecution(DesignName)	#compile for execution with profileing
	SimulateandProfile(DesignName) #simulation instrumented code
	#x = raw_input('please enter a string:')
	BackAnnotate(DesignName)	#back annotate raw profiling data
	SetTop(DesignName, 'Design') #set top level
	#x = raw_input('please enter a string:')
	
	#AllocatePE(DesignName, 'ARM0', 'ARM_7TDMI') #add more here if needed, maybe need a function to change default values of the PEs
	for i in range(numThings):
		AllocatePE(DesignName, PElist[i].name, PElist[i].type)
	#	x= raw_input("alloted?")
	
	MapBehaviorToPE(DesignName, PE0.name, 'Susan') #always allocate susan 
#	MapBehaviorToPE(DesignName, "ARM0", 'Susan') #always allocate susan 

	for i in range(numThings):
		for behavior in PElist[i].behaviors:
			print "mapping ", behavior, " to PE", PElist[i].name
			MapBehaviorToPE(DesignName, PElist[i].name, behavior.capitalize())
		#	x= raw_input("map?")
			

	AnalyzeMapping(DesignName)
	AllocatePE(DesignName, 'VHW0', 'HW_Virtual') #same for all
	AllocatePE(DesignName, 'VHW1', 'HW_Virtual')
	MapBehaviorToPE(DesignName, 'VHW0', 'ReadImage')
	MapBehaviorToPE(DesignName, 'VHW1', 'WriteImage')
	ImportForArch(DesignName)
#	x = raw_input("pauise")
	DesignName = ArchRefinement(DesignName) ## Here is the actual architecture refinement step
	InsProf(DesignName)
	CompileForExecutionNP(DesignName)
	Run(DesignName) #maybe add a check in this function
	CompileForExecution(DesignName)	#compile for execution with profileing
	SimulateandProfile(DesignName) #simulation instrumented code
#	x = raw_input("pauise")
	BackAnnotateandRunEstimation(DesignName) #cretes .ana.sir
	
	#CP = 'cp susan_edge_detectorArch.ana.sirgold susan_edge_detectorArch.ana.sir'
	#os.system(CP)
	#DesignName = DesignName+'Arch'
	#print DesignName
	for i in range(numThings): #change this if you have more processors with Os's
		if PElist[i].type == 'ARM_7TDMI':
			print "mapping os to PE ", i
		#	x = raw_input("pauise")
			SetOS(DesignName, PElist[i].name, PRIOS) #PriOS defined above

	for i in range(numThings): #change this if you have more processors with Os's
		if PElist[i].type == 'ARM_7TDMI':
			pri = 1;
			for behavior in PElist[i].behaviors:
				print "making PE", i, "priority ", pri
		#		x = raw_input("pauise")
				SetPri(DesignName, 'Susan', behavior, str(pri)) #this serializes as well
				pri += 1

#	SetOS(DesignName, 'ARM0', PRIOS) #PriOS defined above
#	SetPri(DesignName, 'Susan', 'edges', '1')
#	SetPri(DesignName, 'Susan', 'thin', '2')
#	SetPri(DesignName, 'Susan', 'draw', '3')
	ImportForSched(DesignName)
	DesignName = 	SchedRefine(DesignName)
	print DesignName
	InsProf(DesignName) #instrument for profiling
	CompileForExecutionNP(DesignName) #compile for execution
	Run(DesignName) #maybe add a check in this function
	#x = raw_input("pauise")
	CompileForExecution(DesignName)	#compile for execution with profileing
	time = SimulateandProfile(DesignName) #simulation instrumented code
	#x = raw_input("pauise")
	print "numThings to allocate = ", numThings
	print "behavior mappings"
	print PE0.behaviors
	print PE1.behaviors
	print PE2.behaviors
	print "PE mappsings"
	for i in range(numThings):
		print "PE" ,i, " mapped to a", PElist[i].type
	for i in range(numThings):
		print "PE" ,i, " is named", PElist[i].name
	#print "Simulation time is ", time
	
	cost = 0
	for i in range(numThings):
		cost += COST[NametoTypeMap[PElist[i].type]]
	print "the cost is ", cost, "the time is" ,time
	print EncodingToString(encoding) 
#	raw_input("end")
	f = open("results.txt", "w")
	f.write(str(time))
	f.write(" ")
	f.write(str(cost))
	return int(time), cost



def EncodingToString(encoding):
		encString = ""
		for i in range(3):
			encString += str(encoding[0][i])
		for i in range(3):
			encString += str(encoding[1][i])
		return encString
			
def MapBehaviorsToPEs(numThings, encoding):
	print numThings
	if numThings == 3:
		PE0.behaviors.append(BehaviorNames[0])
		PE1.behaviors.append(BehaviorNames[1])
		PE2.behaviors.append(BehaviorNames[2])
		PE0.type = TypeMap[encoding[0][0]]
		PE1.type = TypeMap[encoding[0][1]]
		PE2.type = TypeMap[encoding[0][2]]

	if numThings == 2:
		if encoding[1][0] == 0:
				PE0.behaviors.append(BehaviorNames[0])
				PE1.behaviors.append(BehaviorNames[1])
				PE1.behaviors.append(BehaviorNames[2])
				PE0.type = TypeMap[encoding[0][0]]
				PE1.type = TypeMap[encoding[0][1]]
		if encoding[1][0] == 1:
			PE0.behaviors.append(BehaviorNames[0])
			PE0.type = TypeMap[encoding[0][0]]
			if encoding[1][1] == 1:
				PE0.behaviors.append(BehaviorNames[1])
				PE1.behaviors.append(BehaviorNames[2])
				PE1.type = TypeMap[encoding[0][2]]
			else:		
				PE1.behaviors.append(BehaviorNames[1])
				PE0.behaviors.append(BehaviorNames[2])
				PE1.type = TypeMap[encoding[0][1]]
	if numThings == 1:		
		for name in BehaviorNames: #allocate 3 things
			PE0.behaviors.append(name)
			PE0.type = TypeMap[encoding[0][0]]
	for i in range(numThings):
		PE = PElist[i]
		index = NametoTypeMap[PE.type]
		PE.name = NameArrs[index][NumPEs[index]]
		NumPEs[index] += 1

"""
if __name__ == '__main__':
#	encoding = [[1,0,0],[0,1,1]]
#	encoding = [[1,1,1],[1,1,1]]
#	encoding = [[1,0,1],[1,0,1]]
#	encoding = [[1,1,0],[1,1,0]]
#	encoding = [[1,1,0],[0,0,0]]
	encoding = [[1,1,0],[1,1,0]]
	evalSpecFitness(encoding)
"""

#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import random
from deap import base
from deap import creator
from deap import tools
from pareto import paretoBand
from evalFitness import *


creator.create("FitnessMax", base.Fitness, weights=(-1.0,-1.0))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
# Attribute generator
num_mapping = 1 #supports hw_standard and arm (num_mapping+1) = number of processors
toolbox.register("attr_bool", random.randint, 0, 1)
toolbox.register("attr_int", random.randint, 0, num_mapping)
toolbox.register("mapping", tools.initRepeat, list, toolbox.attr_int,3)
toolbox.register("association", tools.initRepeat, list, toolbox.attr_bool,3)
# Structure initializers
toolbox.register("individual", tools.initCycle, creator.Individual, 
    (toolbox.mapping,toolbox.association), 1)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    

def eval2Objective(individual):
    simulation_time =  sum(individual[0])
    cost =  sum(individual[1])
    #return marting_objective(individual)
    #return simulation_time,cost
    return evalSpecFitness(individual)
    
def hw_validate(individual):
    mapping = individual[0]
    association = individual[1]
    
    #make sure all hw_standards arent sharing hw units    
    for i in range(len(mapping)):
        if mapping[i] == 0:
            association[i] = 0
    
    #only simular processors can be shared
    #extract all the occurances of associated behaviours and their indexs 
    ones= [t for t in enumerate(association) if t[1] == 1]
    #get just the index out of the tuple
    ind = [ele[0] for ele in ones]
    #get the mappings for those indexes    
    mapping_ones= [mapping[indx] for indx in ind]
    #if that mapping aren't sharing with simular process types. They arent shared    
    for indx in ind:
        if mapping_ones.count(mapping[indx]) <= 1:
            association[indx] = 0
            
    #reassign to individual
    individual[1]= association
    
#x = list([[3,1,2],[1,1,1]])
#hw_validate(x)
#print(x)
    
indiv = creator.Individual([0,0])
indiv.fitness.values = (0,0)
print("individual %s , %s" % (indiv, indiv.fitness.values))
    
     
# Operator registering
toolbox.register("evaluate", eval2Objective)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate_bool", tools.mutFlipBit, indpb=.5)
toolbox.register("mutate_int", tools.mutUniformInt,low =  0 ,up = num_mapping, indpb=.5)
#toolbox.register("select", tools.selTournament, tournsize = 5)
toolbox.register("hw_validate", hw_validate) 
#toolbox.register("select", tools.selNSGA2)
toolbox.register("select", tools.selSPEA2)

def main():
    random.seed(64)
    all_sim= list()
    pop = toolbox.population(n=15)
    MUTPB, NGEN = .3, 20
    
    print("Start of evolution")
    
    #take out invalids with invalid syntax
    list(map(toolbox.hw_validate, pop))
    # Evaluate the entire population, but don't duplicate evaluate
    for ind in pop:
        if (ind not in all_sim):
            ind.fitness.values = toolbox.evaluate(ind)
            all_sim.append(ind)
        else:
            index = all_sim.index(ind)
            ind.fitness.values = all_sim[index].fitness.values
    
    print("  Evaluated %i individuals" % len(pop))
    
    # Begin the evolution
    for g in range(NGEN):
        print("-- Generation %i --" % g)
        
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop)/2)
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))
    
        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            c1 = toolbox.clone(child1)
            c2 = toolbox.clone(child2)            
            #seperate child1 into two parts                
            c1_mapping = c1[0];
            c1_association= c1[1];
            
            #do the same for chil2 
            c2_mapping = c2[0];
            c2_assocaition= c2[1];
            
            #mate the seperate components
            toolbox.mate(c1_mapping, c2_mapping)
            toolbox.mate(c1_association, c2_assocaition)
            
            #put the seperate children that have been produced back into there childer
            c1[0]= c1_mapping;
            c1[1]= c1_association;
            c2[0]= c2_mapping;
            c2[1]= c2_assocaition;
            
            del c1.fitness.values
            del c2.fitness.values
            
            offspring.append(c1)
            offspring.append(c2)

        for mutant in offspring:
            if random.random() < MUTPB:
                mutant_mapping= mutant[0]
                mutant_asociation= mutant[1]
                toolbox.mutate_int(mutant_mapping)
                toolbox.mutate_bool(mutant_asociation)
                mutant[0]= mutant_mapping
                mutant[1]= mutant_asociation
                del mutant.fitness.values
      
        #validate all the offspring
        list(map(toolbox.hw_validate, offspring))
        
        

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if ((not ind.fitness.valid) and (ind not in all_sim))]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
            
        #add them to the all simulation list
        all_sim.extend(invalid_ind)  
        
        # Fill in fitness value for individuals who have been previously evaluated
        double_ind = [ind for ind in offspring if ((not ind.fitness.valid) and (ind in all_sim))]
        for ind in double_ind:
            idx= all_sim.index(ind)            
            ind.fitness.values = all_sim[idx].fitness.values
        

        
        print("  Evaluated %i individuals" % len(invalid_ind))
        
        # The population is entirely replaced by the offspring
        pop[:] = offspring
        
        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]
        
        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x*x for x in fits)
        std = abs(sum2 / length - mean**2)**0.5
        
        print("  Min %s" % min(fits))
        print("  Max %s" % max(fits))
        print("  Avg %s" % mean)
        print("  Std %s" % std)
    
    print("-- End of (successful) evolution --")
    
    #best individual in final population
    best_ind = tools.selBest(pop, 1)[0]
    print("Best individual is %s, %s" % (best_ind, best_ind.fitness.values))
    
    #print out the entire simulated population and its size
    print("List Of All Evaluated Individuals")    
    for ind in all_sim:
        print("individual %s , %s" % (ind, ind.fitness.values))
    print("Number of Individuals Simulated %s" % (len(all_sim)))
    
    #get the Pareto Band
    pareto_band = paretoBand(all_sim, 2)
    #print out the pareto band
    print("List of Individuals in Pareto Band")  
    for ind in pareto_band:
        print("individual %s , %s" % (ind, ind.fitness.values))
    print"Pareto Band Lenght", len(pareto_band)
    

if __name__ == "__main__":
    main()

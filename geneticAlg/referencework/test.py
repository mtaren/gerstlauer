import os

SCE_IMPORT = 'sce_import'
SRC = '.'
SCE_PATH='SPECC'
SCEDB_PATH=SCE_PATH + '/share/sce/db'
PROCDB_PATH=SCEDB_PATH + '/processors'
BUSDB_PATH=SCEDB_PATH + '/busses'
SYSC_PATH='/opt/pkg/systemc-2.1.v1'
PROC_CACHE='.sce/processors'
COMM_CACHE='.sce/communication'
BUS_CACHE='.sce/busses'
RTL_CACHE='.sce/rtl'
TIME='/usr/bin/time'
SCC='scc'
SCCOPT='-ww -vv -d -xlx -psi -sn'
SCCINC='-I'+SRC+'/common'
#change this SRC
SCCIMP='-P'+SRC+' -P$SRC/common -P$SRC/lp_analysis \
			-P$(SRC)/open_loop -P$(SRC)/closed_loop \
			-P$(SRC)/codebook -P$(SRC)/update -P$(SRC)/processing'
SCCLIB=SRC+'/common/pack.o'
SIR_RENAME='sir_rename'
SIR_RENAMEOPT='-v'

SIR_NOTE='sir_note'
SIR_NOTEOPT='-v'

SIR_STATS='sir_stats'
#SIR_STATSOPT=

SCSH='scsh'
#SCSHOPT		=

SCE_IMPORT='sce_import'
SCE_IMPORTOPT='-v'
SCE_ALLOCATE='sce_allocate'
SCE_ALLOCATEOPT='-v'
SCE_TOP='sce_top'
SCE_TOPOPT='-v'

SCE_MAP='sce_map'
SCE_MAPOPT='-v'

SCE_SCHEDULE='sce_schedule'
SCE_SCHEDULEOPT='-v'

SCE_CONNECT='sce_connect'
SCE_CONNECTOPT='-v'

SCE_PROJECT='sce_project'
SCE_PROJECTOPT='-v'

SCPROF=TIME+' scprof'
SCPROFOPT='-v'

SCAR=TIME+' scar'
#SCAROPT=

SCOS= TIME+' scos'
SCOSOPT= '-v'

SCNR=TIME+' scnr'
#SCNROPT=-v
SCNROPT='-v -O -falign -fmerge'


SCCR=TIME+' sccr'
#SCCROPT='-v'
SCCROPT='-v -O'

SCRTLPP='scrtlpp'
SCRTLPPOPT='-v'

SCRTLSTATS='scrtl_stats'
SCRTLSTATSOPT='-v'

SCRTLBIND='scrtl_bind'
SCRTLBINDOPT='-v'

SCRTLSCHED='scrtl_sched'
SCRTLSCHEDOPT='-v'

SCRTL=TIME+' scrtl'
SCRTLOPT='-v'

SC2C=TIME+' sc2c'
SC2COPT='-v'

SC2SYSC='sc2sysc'
SC2SYSCOPT='-v -psi'

RM='rm -f'
CP='cp -f'
MV='mv -f'
TAR='gtar'
MKDIR='mkdir -p'
TOUCH='touch'
DIFF='diff -s'
ECHO='echo'



CP = 'cp susan_egde_detectorArch.ana.sir.gold susan_edge_detectorArch.ana.sir'
os.system(CP)

#set os, and priorities
CMD = 'sce_schedule -v -k ARM0=ARM_7TDMI_OS_PRIORITY_20000_0 -t Susan -p edges=1 -p thin=2 -p draw=3 -i susan_edge_detectorArch.ana.sir -o susan_edge_detectorArch.sir susan_edge_detectorArch'
os.system(CMD)
#no serialize each behavior. Edges
CMD = 'sce_schedule -v -t Edges -s -r -i susan_edge_detectorArch.sir -o susan_edge_detectorArch.sir susan_edge_detectorArch'
os.system(CMD)
#Thin
CMD = 'sce_schedule -v -t Thin -s -r -i susan_edge_detectorArch.sir -o susan_edge_detectorArch.sir susan_edge_detectorArch'
os.system(CMD)
#Draw
CMD = 'sce_schedule -v -t Draw -s -r -i susan_edge_detectorArch.sir -o susan_edge_detectorArch1.sir susan_edge_detectorArch'
os.system(CMD)
#import componants for scheduling refinement
Name = 'susan_edge_detectorArch'
CMD = SCE_IMPORT + ' -s '+SCE_IMPORTOPT + ' -i ' +  Name + '.sir -o ' + Name + '.sched.in.sir '+ Name
os.system(CMD)

#static scheduling refinement 
CMD = SCAR + ' ' + Name + ' -s  -i ' + Name + '.sched.in.sir -o ' + Name + '.sched.tmp.sir '
os.system(CMD)

#dynamic scheduling refinement
CMD = SCAR + ' ' + Name + '  -i ' + Name + '.sched.tmp.sir -o ' + Name + '.sched.tmp.sir '
os.system(CMD)


print 'now check'
CMD = 'sce_schedule -v -k ARM0 -i susan_edge_detectorArch.sir -o susan_edge_detectorArch1.sir susan_edge_detectorArch'
os.system(CMD)

#include "susan.sh"

import "c_handshake";
import "c_double_handshake";
import "design";
import "stimulus";
import "monitor";


behavior Main
{
    sim_time start_time;
    uchar image_buffer[IMAGE_SIZE];
    c_handshake start;
    c_handshake finish;
    c_double_handshake out_image;
    
    Stimulus stimulus(start, image_buffer, finish, start_time);
    Design design(start, image_buffer, out_image, finish);
    Monitor monitor(out_image, finish, start_time);
    
    int main(void) {
        par {
            stimulus;
            design;
            monitor;
        }
        
        return 0;
    }

};

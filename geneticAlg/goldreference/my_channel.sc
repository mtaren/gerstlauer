interface IS
{
    void Send(unsigned int);
};

interface IR
{
    unsigned int Receive(void);
};

channel my_channel implements IS, IR
{
    event Req;
    event Ack;
    unsigned int DATA;
    
    void Send(unsigned int d)
    {
        DATA = d;
        notify Req;
        wait Ack;
    }
    
    unsigned int Receive(void)
    {
        unsigned int d;
        
        wait Req;
        d = DATA;
        notify Ack;
        return d;
    }
};


import os
SRC = '.'
SCE_PATH='SPECC'
SCEDB_PATH=SCE_PATH + '/share/sce/db'
PROCDB_PATH=SCEDB_PATH + '/processors'
BUSDB_PATH=SCEDB_PATH + '/busses'
SYSC_PATH='/opt/pkg/systemc-2.1.v1'
PROC_CACHE='.sce/processors'
COMM_CACHE='.sce/communication'
BUS_CACHE='.sce/busses'
RTL_CACHE='.sce/rtl'
TIME='/usr/bin/time'
SCC='scc'
SCCOPT='-ww -vv -d -xlx -psi -sn'
SCCINC='-I'+SRC+'/common'
#change this SRC
SCCIMP='-P'+SRC+' -P$SRC/common -P$SRC/lp_analysis \
			-P$(SRC)/open_loop -P$(SRC)/closed_loop \
			-P$(SRC)/codebook -P$(SRC)/update -P$(SRC)/processing'
SCCLIB=SRC+'/common/pack.o'
SIR_RENAME='sir_rename'
SIR_RENAMEOPT='-v'

SIR_NOTE='sir_note'
SIR_NOTEOPT='-v'

SIR_STATS='sir_stats'
#SIR_STATSOPT=

SCSH='scsh'
#SCSHOPT		=

SCE_IMPORT='sce_import'
SCE_IMPORTOPT='-v'
SCE_ALLOCATE='sce_allocate'
SCE_ALLOCATEOPT='-v'
SCE_TOP='sce_top'
SCE_TOPOPT='-v'

SCE_MAP='sce_map'
SCE_MAPOPT='-v'

SCE_SCHEDULE='sce_schedule'
SCE_SCHEDULEOPT='-v'

SCE_CONNECT='sce_connect'
SCE_CONNECTOPT='-v'

SCE_PROJECT='sce_project'
SCE_PROJECTOPT='-v'

SCPROF=TIME+' scprof'
SCPROFOPT='-v'

SCAR=TIME+' scar'
#SCAROPT=

SCOS= TIME+' scos'
SCOSOPT= '-v'

SCNR=TIME+' scnr'
#SCNROPT=-v
SCNROPT='-v -O -falign -fmerge'


SCCR=TIME+' sccr'
#SCCROPT='-v'
SCCROPT='-v -O'

SCRTLPP='scrtlpp'
SCRTLPPOPT='-v'

SCRTLSTATS='scrtl_stats'
SCRTLSTATSOPT='-v'

SCRTLBIND='scrtl_bind'
SCRTLBINDOPT='-v'

SCRTLSCHED='scrtl_sched'
SCRTLSCHEDOPT='-v'

SCRTL=TIME+' scrtl'
SCRTLOPT='-v'

SC2C=TIME+' sc2c'
SC2COPT='-v'

SC2SYSC='sc2sysc'
SC2SYSCOPT='-v -psi'

RM='rm -f'
CP='cp -f'
MV='mv -f'
TAR='gtar'
MKDIR='mkdir -p'
TOUCH='touch'
DIFF='diff -s'
ECHO='echo'


##-------------- starting code here
#NAME = 'susan_edge_detector'
VERBOSE = True
#CLEAN = True
CLEAN = False
INS ='.ins.sir'

##assumed already compiled code, susanedgedetector.sir
#Makefile equivalent to Instrumenting $* for profiling
#creates a .ins.sir file from the SPEC.sir file
# $< is prerequisite/source file susan_edge_detector.sir
# $@ means file being generated  susan_edge_detector.ins.sir
# $* is target, or  susan_edge_detector
#input is susanedgedetector.sir, outputs SED.ins.sir
#pass in design name
def InsProf(NAME):
	if CLEAN==True:
		os.system('rm *.ins.sir') #clean
	CMD = SCPROF + ' -m ' + SCPROFOPT + ' -i ' + NAME + '.sir -o ' + NAME+'.ins.sir' + ' '+ NAME
	print 'instrumenting for profiling'
	if VERBOSE==True:
		print CMD
	os.system(CMD)


#Makefile equivalent to "*** Compiling $* for execution (no profiling)"
#creates .sir.ins file from the .ins file created above
#input is the .ins.sir file above, outsputs .ins file
#DO WE ACTUALLY NEED THIS? THIS JUST RUNS IT TO SEE IF IT WORKS 
#os.system('rm ' + NAME+ '.ins') #clean
#CMD = 'scc '+ NAME + ' -sir2out ' + SCCOPT + ' -i ' + NAME + '.ins.sir -o ' + NAME + '.ins' 
#print 'instrumenting for profiling'
#if VERBOSE==True:
#	print CMD
#os.system(CMD)

#Makefile equivalent to "*** Compiling $* for execution with profiling..."
#creates .sir.ins file from the .ins file created above
#input is the .ins.sir file above, outsputs .ins file
def CompileForExecution(NAME):
	if CLEAN==True:
		os.system('rm ' + NAME+ '.ins') #clean
	CMD = 'scc '+ NAME + ' -sir2out ' + SCCOPT + ' -i ' + NAME + '.ins.sir -o ' + NAME + '.ins' 
	print 'instrumenting for profiling'
	if VERBOSE==True:
		print CMD
	os.system(CMD)


#Makefile equivalent to simulating the .ins file
#Need to check if successful, also check if deadlock detected. This will be tuff if it
#just runs forever or somthing. Maybe have timeouts?
def SimulateandProfile(NAME):
	if CLEAN==True:
		os.system(' rm *_bb_counter.prf')
		os.system(' rm *_fn_counter.prf')
	CMD = './' + NAME + '.ins'
	if VERBOSE==True:
		print 'simulating .ins file, creates _bb and _fn .prf'
		print CMD
	os.system(CMD)

#Makefile Equivalanet to back-annotate raw profiling data
#Takes in SED.sir, outputs SED.prof.sir
def  BackAnnotate(NAME):
	CMD = SCPROF + ' -p ' + SCPROFOPT + ' -i ' + NAME + '.sir -o  ' + NAME + '.prof.sir ' + NAME
	if CLEAN==True:
		os.system('rm *.prof.sir')
	if VERBOSE==True:
		print "Makefile Equivalanet to back-annotate raw profiling data"
		print CMD
	os.system(CMD)

#Makefile Equivalent to set top level of design under test
#input is the .prof.sir file, outputs new sir file. 
#TOP = 'Design'
def SetTop(NAME, TOP):
	CMD = SCE_TOP + ' ' + SCE_TOPOPT + ' -s ' + TOP + '  -i ' + NAME + '.prof.sir -o ' + NAME + '.sir ' + NAME
	if CLEAN==True:
		os.system('rm *.2.sir -f')
	if VERBOSE==True:
		print '#Makefile Equivalent to set top level of design under test'
		print CMD
	os.system(CMD)


#Makefile Equivalent to Allocateing a PE.
#pass in the 2.sir made above, outputs 3.sir
#TYPE = 'ARM_7TDMI' #using all default valuse
#TYPE = 'HW_Standard'
def AllocatePE(NAME, PENAME, TYPE):
	#PENAME = 'ARM0'
	if CLEAN==True:
		os.system('rm *.3.sir -f')
	CMD = SCE_ALLOCATE + ' ' + SCE_ALLOCATEOPT + ' -p ' + PENAME  +'=' + TYPE + ' -i ' + NAME+ '.sir -o ' + NAME + '.sir ' + NAME
	if VERBOSE==True:
		print 'allocate an' + PENAME + 'pe'
		print CMD
	os.system(CMD)

#Makefile Equivalent to now map top level -"design" to arm above
#pass int he 3.sir file, spits out 4.sir
def MapBehaviorToPE(NAME, PENAME, BEHAVIORNAME):
	if CLEAN==True:
		os.system('rm *.4.sir -f')
#	BEHAVIORNAME = 'Design'
#	PENAME = 'ARM0'
	CMD = SCE_MAP + ' ' + SCE_MAPOPT + ' -p ' + BEHAVIORNAME +'=' + PENAME + ' -i ' + NAME + '.sir -o ' + NAME + '.sir ' + NAME
	if VERBOSE==True:
		print 'map design to ARM0' 
		print CMD
	os.system(CMD)


#Makefile equivalent  to analyze the profile given the SW mapping
#takes in a .4.sir and outputs .anal.sir
#do this once everything is allocated/mapped
def AnalyzeMapping(NAME):
	if CLEAN==True:
		os.system('rm *.anal.sir')
	CMD = SCPROF + ' -a ' + SCPROFOPT + '  -i ' + NAME + '.sir -o ' + NAME + '.sir ' + NAME
	if VERBOSE==True:
		print 'analyze the mapping, make anal.sir ' 
		print CMD
	os.system(CMD)

## Get ready for arch refinement by importing PES to the design
## this creates an .arch.in.sir file
def ImportForArch(NAME):
	if CLEAN==True:
		os.system('rm *.arch.in.sir')
	CMD = SCE_IMPORT + ' ' + SCE_IMPORTOPT + ' -a -i ' + NAME + '.sir -o ' +NAME + '.arch.in.sir ' + NAME 
	if VERBOSE==True:
		print 'import the PES into the design for architecture refinement' 
		print CMD
	os.system(CMD)

def ArchRefinement(Name):
	CMD = SCAR + ' ' + Name +' -b -m -c -w -i ' + Name + '.arch.in.sir -o ' + Name + '.arch.sir'
	if VERBOSE==True:
		print 'performing arch refinement. ' 
		print CMD
	os.system(CMD)
	CMD = SIR_RENAME + ' ' + SIR_RENAMEOPT + ' -i ' +  Name + '.arch.sir ' + Name + ' ' + Name + 'Arch'
	if VERBOSE==True:
		print 'performing arch refinement. ' 
		print CMD
	os.system(CMD)
	return Name + 'Arch'


####################################### Architecture  refinement fcns
def CompileForExecutionNP(Name):
	CMD = SCC + ' '+Name + ' -sir2out ' 
	if VERBOSE==True:
		print  'compiling executable using sir2out' 
		print CMD
	os.system(CMD)

#runs the file passed in
def Run(Name):
	CMD = './' + Name
	if VERBOSE==True:
		print  'running Name' 
		print CMD
	os.system(CMD)

def CheckSimOutput(): # add an expect in here
	CMD = 'diff -s  golden.pgm out.pgm'
	if VERBOSE==True:
		print  'Checking output'
		print CMD
	os.system(CMD)
	

	
## --- back-annotate profiling data and run estimation
def BackAnnotateandRunEstimation(Name): #assumes simulation is ok
	CMD = SCPROF + ' -E ' + SCPROFOPT + ' -i ' + Name + '.sir -o ' + Name + '.ana.sir ' + Name
	if VERBOSE==True:
		print  'back-annotate profiling data and run estimation' 
		print CMD
	os.system(CMD)

##################################Sche refinement fcn
#set os, and priorities
PRIOS = 'ARM_7TDMI_OS_PRIORITY_20000_0'

def SetOS(Name, PE, OS): #Sets the os of a PE
	CMD = 'sce_schedule -v -k '+ PE+ '=' + OS + ' -i ' + Name +'.ana.sir -o '+ Name + '.sir '+ Name
	if VERBOSE==True:
		print CMD
	os.system(CMD)
def SetPri(Name, TopB, ChildB, Pri): #Sets the Priority of somthing like 'Susan' 'edges=1'
	CMD = 'sce_schedule -v -t ' + TopB + ' -p '+ ChildB + '='+ Pri +' -i ' + Name + '.sir -o ' + Name + '.sir ' + Name
	if VERBOSE==True:
		print CMD
	os.system(CMD)

def SeriliazeBehavior(Name,Behavior): #now serialize each behavior. Edges
	CMD = 'sce_schedule -v -t '+  Behavior +' -s -r -i ' + Name + '.sir -o ' + Name + '.sir ' + Name
	if VERBOSE==True:
		print CMD
	os.system(CMD)

def ImportForSched(Name): #import componants for scheduling refinement
	CMD = SCE_IMPORT + ' -s '+SCE_IMPORTOPT + ' -i ' +  Name + '.sir -o ' + Name + '.sched.in.sir '+ Name
	if VERBOSE==True:
		print CMD
	os.system(CMD)

def SchedRefine(Name): #static and dynamic scheduling refinement 
	CMD = SCAR + ' ' + Name + ' -s  -i ' + Name + '.sched.in.sir -o ' + Name + '.sched.tmp.sir '
	if VERBOSE==True:
		print CMD
	os.system(CMD)
	CMD = SCOS + ' ' + SCOSOPT + ' '  + Name + '  -i ' + Name + '.sched.tmp.sir -o ' + Name + '.sched.sir '
	os.system(CMD)
	if VERBOSE==True:
		print CMD
	CMD = SIR_RENAME + ' ' + SIR_RENAMEOPT + ' -i ' + Name + '.sched.sir ' + Name + ' ' + Name[:-4] + 'Sched'
	if VERBOSE==True:
		print CMD
	os.system(CMD)
	return Name[:-4] + 'Sched'

	
#need to change the processor default values?
#need to make a better check simulation output with an expect or somthing
#need to pull the simulation value out too

if __name__ == '__main__':
	DesignName = 'susan_edge_detector'
	os.system('cp ../susan_edge_detector.sir .')
	InsProf(DesignName) #instrument for profiling
						#compile for execution
	CompileForExecutionNP(DesignName)
	Run(DesignName) #maybe add a check in this function
	#x = raw_input('please enter a string:')
	CompileForExecution(DesignName)	#compile for execution with profileing
	SimulateandProfile(DesignName) #simulation instrumented code
	#x = raw_input('please enter a string:')
	BackAnnotate(DesignName)	#back annotate raw profiling data
	SetTop(DesignName, 'Design') #set top level
	#x = raw_input('please enter a string:')
	AllocatePE(DesignName, 'ARM0', 'ARM_7TDMI') #add more here if needed, maybe need a function to change default values of the PEs
	MapBehaviorToPE(DesignName, 'ARM0', 'Susan')
	AnalyzeMapping(DesignName)
	AllocatePE(DesignName, 'VHW0', 'HW_Virtual')
	AllocatePE(DesignName, 'VHW1', 'HW_Virtual')
	MapBehaviorToPE(DesignName, 'VHW0', 'ReadImage')
	MapBehaviorToPE(DesignName, 'VHW1', 'WriteImage')
	ImportForArch(DesignName)
#	x = raw_input("pauise")
	DesignName = ArchRefinement(DesignName) ## Here is the actual architecture refinement step
	InsProf(DesignName)
	CompileForExecutionNP(DesignName)
	Run(DesignName) #maybe add a check in this function
	CompileForExecution(DesignName)	#compile for execution with profileing
	SimulateandProfile(DesignName) #simulation instrumented code
	x = raw_input("pauise")
	BackAnnotateandRunEstimation(DesignName) #cretes .ana.sir
	
	#CP = 'cp susan_edge_detectorArch.ana.sirgold susan_edge_detectorArch.ana.sir'
	#os.system(CP)
	#DesignName = DesignName+'Arch'
	#print DesignName
	SetOS(DesignName, 'ARM0', PRIOS) #PriOS defined above
	SetPri(DesignName, 'Susan', 'edges', '1')
	SetPri(DesignName, 'Susan', 'thin', '2')
	SetPri(DesignName, 'Susan', 'draw', '3')
	ImportForSched(DesignName)
	DesignName = 	SchedRefine(DesignName)
	print DesignName
	InsProf(DesignName) #instrument for profiling
	CompileForExecutionNP(DesignName) #compile for execution
	Run(DesignName) #maybe add a check in this function
	x = raw_input("pauise")
	CompileForExecution(DesignName)	#compile for execution with profileing
	SimulateandProfile(DesignName) #simulation instrumented code
	x = raw_input("pauise")
	BackAnnotate(DesignName)	#back annotate raw profiling data

	
	

"""
#Makefile Equivalanet to back-annotate profiling data and run estimation
CMD = SCPROF + ' -E ' + SCPROFOPT + ' -i ' + NAME + '.sir -o  ' + NAME + '.ana.sir ' + NAME
if VERBOSE==True:
	print "Makefile Equivalanet to back-annotate profiling data and run estimation"
	print CMD
os.system(CMD)"""


"""Steps
# --- compile the sources
VocoderSpec.sir:	$(SRC)/testbench.sc

# --- instrument for profiling
VocoderSpec.ins.sir:	VocoderSpec.sir

# --- opt.: compile for execution
VocoderSpec:		VocoderSpec.sir

# --- opt.: simulate using dtx mode
VocoderSpec.dtx.bit:	VocoderSpec

# --- opt.: check simulation (dtx)
VocoderSpec.DTX_OK:	VocoderSpec.dtx.bit

# --- compile for execution with profiling
VocoderSpec.ins:	VocoderSpec.ins.sir

# --- simulate instrumented code using nodtx mode
VocoderSpec.nodtx.bit:	VocoderSpec.ins

# --- check simulation (nodtx)
VocoderSpec.NODTX_OK:	VocoderSpec.nodtx.bit

# --- back-annotate raw profiling data
VocoderSpec.prof.sir:	VocoderSpec.sir VocoderSpec.NODTX_OK
	@$(ECHO) "***"
	@$(ECHO) "*** Back-annotating raw profiling data..."
	@$(ECHO) "***"
	$(SCPROF) -p $(SCPROFOPT) -i VocoderSpec.sir -o VocoderSpec.prof.sir \
		VocoderSpec
"""

<!DOCTYPE SCE>
<sce>
 <project>lab2.sce</project>
 <compiler>
  <option name="libpath" ></option>
  <option name="libs" ></option>
  <option name="incpath" ></option>
  <option name="importpath" ></option>
  <option name="defines" ></option>
  <option name="undefines" ></option>
  <option verbosity="3" warning="2" name="options" >-v</option>
 </compiler>
 <simulator>
  <option type="0" name="tracing" calls="False" debug="False" ></option>
  <option type="2" name="terminal" >xterm -title %e -e</option>
  <option name="logging" enabled="False" >.log</option>
  <option name="command" >/usr/bin/time ./%e &amp;&amp; diff -s output_edge.pgm golden.pgm</option>
 </simulator>
 <models/>
 <imports/>
 <sources/>
 <metrics>
  <option name="types" ></option>
  <option name="operations" ></option>
 </metrics>
</sce>

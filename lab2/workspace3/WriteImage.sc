/****************************************************************************
*  Title: WriteImage.sc
*  Author: Dongwan Shin
*  Date: 04/20/2001
*  Description: Specification model for even parity checker
****************************************************************************/
import "c_queue";
import "imageType";
#define XSIZE 76
#define YSIZE 95

behavior WriteImage(i_uImage_receiver inq, i_uImage_sender outq)
{
	void main(void) {
		struct image data;

		while (1) {
			inq.receive(&data);
			outq.send(data);
		}
	}
};
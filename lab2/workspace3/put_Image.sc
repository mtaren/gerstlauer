#include <sys/file.h>
#include <stdlib.h>
#include <stdio.h>
//#include "get_Image.h"

#define XSIZE 76
#define YSIZE 95
#define  exit_error(IFB,IFC) { fprintf(stderr,IFB,IFC); exit(0); }
behavior put_Image(in int d){
void main(void){
return;
}
put_image(char  *filename,unsigned char in_image [XSIZE*YSIZE])
{
FILE  *fd;

#ifdef FOPENB
  if ((fd=fopen(filename,"wb")) == NULL) 
#else
  if ((fd=fopen(filename,"w")) == NULL) 
#endif
    exit_error("Can't output image%s.\n",filename);

  fprintf(fd,"P5\n");
  fprintf(fd,"%d %d\n",XSIZE,YSIZE);
  fprintf(fd,"255\n");
  
  if (fwrite(in_image,XSIZE*YSIZE,1,fd) != 1)
    exit_error("Can't write image %s.\n",filename);

  fclose(fd);
}
};

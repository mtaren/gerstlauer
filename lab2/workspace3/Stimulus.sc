/****************************************************************************
*  Title: io.sc
*  Author: Dongwan Shin
*  Date: 04/20/2001
*  Description: testbench
****************************************************************************/

#include <stdio.h>
#include <sys/file.h>
#include <stdlib.h>
#define XSIZE 76
#define YSIZE 95
#define  exit_error(IFB,IFC) { fprintf(stderr,IFB,IFC); exit(0); }
#include <sim.sh>
import "c_queue";
//import "DEFINES"; nope
#define NUMPICS 5 //change this in monitor too... cant figure out how to share this define

void get_image(char* dee, unsigned char  d[XSIZE*YSIZE]);
int getint(FILE *fd);
import "imageType";

behavior Stimulus(i_uImage_sender inq)
{
	
	void main(void) {
		struct image in_image;
		char filename[55] = "input_small.pgm";
		char * p;
		sim_time_string	buf; 
		int i;

//		while (1) {
			get_image("input_small.pgm", in_image.imagePack);
			//get the sim time		 
			for(i = 0; i < NUMPICS; i++){
				waitfor(1000);
			//	printf("i = %d\n", i);
				printf("Time%4s: Sending Image\n", time2str(buf, now()));
				inq.send(in_image);	
			}
	}
};


int getint(FILE *fd){
  int c, i;
  char dummy[10000];

  c = getc(fd);
  while (1) 
  {
    if (c=='#')   
      fgets(dummy,9000,fd);
    if (c==EOF)
      exit_error("Image %s not binary PGM.\n","is");
    if (c>='0' && c<='9')
      break;   
    c = getc(fd);
  }

    i = 0;
  while (1) {
    i = (i*10) + (c - '0');
    c = getc(fd);
    if (c==EOF) return (i);
    if (c<'0' || c>'9') break;
  }

  return (i);
}
void get_image(char* filename,unsigned char in_image[XSIZE*YSIZE]){

FILE  *fd;
char header [100];
int  tmp;

#ifdef FOPENB
  if ((fd=fopen(filename,"rb")) == NULL)
#else
  if ((fd=fopen(filename,"r")) == NULL)
#endif
    exit_error("Can't input image %s.\n",filename);

    header[0]=fgetc(fd);
  header[1]=fgetc(fd);
  if(!(header[0]=='P' && header[1]=='5'))
    exit_error("Image %s does not have binary PGM header.\n",filename);

  getint(fd);// These two are needed for some reason,
  getint(fd); //probably to move the file pointer along
  tmp = getint(fd);


 // *in = (uchar *) malloc(*XSIZE * *YSIZE);

  if (fread(in_image,1,XSIZE * YSIZE,fd) == 0)
    exit_error("Image %s is wrong size.\n",filename);
  fclose(fd);

}


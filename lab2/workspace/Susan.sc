/****************************************************************************
*  Title: parity.sc
*  Author: Dongwan Shin
*  Date: 04/20/2001
*  Description: Specification model for even parity checker
****************************************************************************/
#include <stdio.h>
#define XSIZE 76
#define YSIZE 95
#include <sim.sh>

import "c_handshake";
import "c_queue";
import "edge_draw.sc";
import "susan_thin.sc";
import "susan_edges.sc";
import "setup_brightness_lut.sc";
import "imageType";
import "RimageType"; //for array R

behavior Susan(i_uImage_receiver inq, i_uImage_sender outq)
{
	//Shared stuff-=-==-=--==-=--==-=--=-=
/*	unsigned char mid[XSIZE*YSIZE];
	unsigned char in_image[XSIZE*YSIZE];
	unsigned char  bpt[516]; 
	int    r[XSIZE*YSIZE];
	int bptIndex = 258;	*/
	

//replace with typed queues //this will probably be 1?
//	unsigned long const size = XSIZE*YSIZE;
	unsigned long const size2 = XSIZE*YSIZE;//because r is an int, 1+ 4
	unsigned long const size = size2;

	//--------------
//   c_queue SEdge_to_EdgeDraw(size); //just in_image
   c_uImage_queue SEdge_to_EdgeDraw(size); //just in_image
//   c_queue SEdge_to_Sthin(size2); //r and mid
   c_uImage_queue SEdge_to_Sthin(size); //mid
	 c_intImage_queue SEdge_to_SthinR(size); //r
//	 c_queue Sthin_to_edge_draw2(size); //just mid
	 c_uImage_queue Sthin_to_edge_draw(size);
	 
		

	//instantiate the behaviours
//	setup_brightness_lut lut(bpt); //absorbed by susan edges
//	susan_edges sus_edg(in_image,r,mid,bpt, bptIndex); changed to queue
	susan_edges_fsm sus_edg(inq,SEdge_to_EdgeDraw, SEdge_to_Sthin,SEdge_to_SthinR );
	SusanThin_fsm susan_thin(SEdge_to_Sthin, SEdge_to_SthinR ,Sthin_to_edge_draw);
	EdgeDraw_fsm edge_draw(outq, SEdge_to_EdgeDraw, Sthin_to_edge_draw); //changed to queu
//	EdgeDraw edge_draw(in_image,mid, outq);

	void main(void) {
	int i;
	//need to move this as well
//	inq.receive(in_image, sizeof(in_image) );
		
    par{
  //  lut: goto sus_edg;
    susan_thin;
    edge_draw;
    sus_edg;
    }

    }
};








/****************************************************************************
*  Title: ReadImage.sc
*  Author: Dongwan Shin
*  Date: 04/20/2001
*  Description: Specification model for even parity checker
****************************************************************************/
import "c_queue";
import "imageType";
#define XSIZE 76
#define YSIZE 95

behavior ReadImage(i_uImage_receiver inq, i_uImage_sender outq)
{
	void main(void) {
		//unsigned char data[XSIZE*YSIZE];
		struct image data;

		while (1) {
			inq.receive(&data);
			outq.send(data);
		}
	}
};
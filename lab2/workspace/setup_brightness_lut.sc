/****************************************************************************
*  Title: Edge_Draw.sc
*  Author: Lucas Holt
*  Date: 2/3/2014
*  Description: Specification model for even parity checker
****************************************************************************/
#define XSIZE 76
#define YSIZE 95
#include <math.h>
behavior setup_brightness_lut(inout unsigned char bp [516])
{
	void main(void) {
        int   k;
        float temp;
        int index = 258;
        
         // bp=(unsigned char *)malloc(516);
        //  bp=bp+258;
        
          for(k=-256;k<257;k++)
          {
            temp=((float)k)/((float)20);
            temp=temp*temp;
         //   if (form==6) always 6
            temp=temp*temp*temp;
            temp=100.0*exp(-temp);
            *(bp+k+index)= (unsigned char)temp;
          }

	}
};

<!DOCTYPE SCE>
<sce>
 <project>lab2workspace.sce</project>
 <compiler>
  <option name="libpath" ></option>
  <option name="libs" ></option>
  <option name="incpath" ></option>
  <option name="importpath" >.</option>
  <option name="defines" ></option>
  <option name="undefines" ></option>
  <option verbosity="3" name="options" warning="2" >-v</option>
 </compiler>
 <simulator>
  <option type="0" name="tracing" debug="False" calls="False" ></option>
  <option type="2" name="terminal" >xterm -title %e -e</option>
  <option name="logging" enabled="False" >.log</option>
  <option name="command" >/usr/bin/time ./%e &amp;&amp; diff -s out.pgm goldgen.pgm</option>
 </simulator>
 <models>
  <item type="" name="susanEdgeDetect_top.sir" />
 </models>
 <imports>
  <item name="Design" />
  <item name="Monitor" />
  <item name="ReadImage" />
  <item name="RimageType" />
  <item name="Stimulus" />
  <item name="Susan" />
  <item name="WriteImage" />
  <item name="c_handshake" />
  <item name="c_queue" />
  <item name="edge_draw" />
  <item name="edge_draw_thread_bott" />
  <item name="edge_draw_thread_top" />
  <item name="i_receive" />
  <item name="i_receiver" />
  <item name="i_send" />
  <item name="i_sender" />
  <item name="i_tranceiver" />
  <item name="imageType" />
  <item name="setup_brightness_lut" />
  <item name="susan_edges" />
  <item name="susan_thin" />
 </imports>
 <sources>
  <item name="./Design.sc" />
  <item name="./Monitor.sc" />
  <item name="./ReadImage.sc" />
  <item name="./RimageType.sc" />
  <item name="./Stimulus.sc" />
  <item name="./Susan.sc" />
  <item name="./WriteImage.sc" />
  <item name="./edge_draw.sc" />
  <item name="./edge_draw_thread_bott.sc" />
  <item name="./edge_draw_thread_top.sc" />
  <item name="./imageType.sc" />
  <item name="./setup_brightness_lut.sc" />
  <item name="./susan_edges.sc" />
  <item name="./susan_thin.sc" />
  <item name="/usr/include/_G_config.h" />
  <item name="/usr/include/alloca.h" />
  <item name="/usr/include/bits/fcntl.h" />
  <item name="/usr/include/bits/mathcalls.h" />
  <item name="/usr/include/bits/sigset.h" />
  <item name="/usr/include/bits/sys_errlist.h" />
  <item name="/usr/include/bits/time.h" />
  <item name="/usr/include/bits/types.h" />
  <item name="/usr/include/fcntl.h" />
  <item name="/usr/include/gconv.h" />
  <item name="/usr/include/libio.h" />
  <item name="/usr/include/math.h" />
  <item name="/usr/include/stdio.h" />
  <item name="/usr/include/stdlib.h" />
  <item name="/usr/include/string.h" />
  <item name="/usr/include/sys/file.h" />
  <item name="/usr/include/sys/select.h" />
  <item name="/usr/include/sys/types.h" />
  <item name="/usr/include/time.h" />
  <item name="/usr/include/wchar.h" />
  <item name="/usr/lib/gcc/i386-redhat-linux/3.4.6/include/stdarg.h" />
  <item name="/usr/lib/gcc/i386-redhat-linux/3.4.6/include/stddef.h" />
  <item name="/usr/local/packages/sce-20100908/inc/bits/huge_val.h" />
  <item name="/usr/local/packages/sce-20100908/inc/bits/pthreadtypes.h" />
  <item name="/usr/local/packages/sce-20100908/inc/sim.sh" />
  <item name="/usr/local/packages/sce-20100908/inc/sim/bit.sh" />
  <item name="/usr/local/packages/sce-20100908/inc/sim/longlong.sh" />
  <item name="/usr/local/packages/sce-20100908/inc/sim/time.sh" />
  <item name="c_handshake.sc" />
  <item name="c_queue.sc" />
  <item name="i_receive.sc" />
  <item name="i_receiver.sc" />
  <item name="i_send.sc" />
  <item name="i_sender.sc" />
  <item name="i_tranceiver.sc" />
  <item name="susan_edge_detector.sc" />
 </sources>
 <metrics>
  <option name="types" >char[10000],pthread_barrier_t,unsigned long int[2],unsigned long long int,fd_set,char[48],int,struct flock,float,char[40],int[2],char,pthread_mutex_t,struct __pthread_internal_slist,struct _IO_marker,__fsid_t,char[1],pthread_mutexattr_t,struct __gconv_step_data,pthread_cond_t,unsigned short int,long long int,event,char[100],__sigset_t,int[7220],short int,struct drand48_data,long double,bit[64],struct __gconv_info,div_t,pthread_condattr_t,pthread_barrierattr_t,struct random_data,bool,int[9],char[24],__u_quad_t,ldiv_t,unsigned short int[7],struct exception,void,char[32],__mbstate_t,unsigned long int[32],unsigned char[516],void*,struct _IO_FILE,long int[2],unsigned char[7220],unsigned char,_G_iconv_t,struct __gconv_step,char[4],char[21],struct timespec,long int,pthread_rwlockattr_t,char[55],__huge_val_t,unsigned int,struct __gconv_trans_data,unsigned char[8],struct timeval,struct __gconv_step_data[1],unsigned long int,long int[32],double,unsigned short int[3],struct image,struct Rimage,enum __codecvt_result,_G_fpos_t,__quad_t,pthread_attr_t,pthread_rwlock_t,_G_fpos64_t,char[36],char[20],_LIB_VERSION_TYPE,char[8]</option>
  <option name="operations" >get_image,getint,put_image</option>
 </metrics>
</sce>

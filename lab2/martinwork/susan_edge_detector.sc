/****************************************************************************
top behavior
****************************************************************************/
//import "io";
//import "parity";
import "Design";
import "Monitor";
import "Stimulus";
import "c_handshake";
import "c_queue";
import "imageType";
#define XSIZE 76
#define YSIZE 95
#define THRESH 20
#define MAXEDGES 2650

behavior Main
{
	//c_handshake startShake;
	//unsigned char in_image[XSIZE*YSIZE];
	unsigned long const size = 1;
	//--------------
    c_uImage_queue outq (size);
	c_uImage_queue inq(size);
	
	Stimulus stim(inq);
	Design design(inq, outq);
	Monitor mon(outq); 

	int main (void)
	{
		par {
			stim.main();
			design.main();
			mon.main();
		}
		return 0;
	}
};

/****************************************************************************
top behavior
****************************************************************************/
import "WriteImage";
import "ReadImage";
import "Susan";
import "imageType";
import "c_queue";
import "imageType";
#define XSIZE 76
#define YSIZE 95

behavior Design(i_uImage_receiver inq, i_uImage_sender outq)
{
	//c_handshake startShake;
	//unsigned char in_image[XSIZE*YSIZE];
	unsigned long const size = 1;
	//--------------
    c_uImage_queue sub_outq (size);
	c_uImage_queue sub_inq (size);
	bit[64] d;	
	ReadImage read(inq, sub_inq); 
	Susan susan(sub_inq, sub_outq);
	WriteImage write(sub_outq, outq);

	void main (void)
	{
		par {
			read.main();
			susan.main();
			write.main();
		}
	}
};

#include <math.h>
#include <stdio.h>
#include <string.h>
import "c_handshake";
import "c_queue";
#define XSIZE 76
#define YSIZE 95
#define THRESH 20
#define MAXEDGES 2650

import "imageType";
import "RimageType";

//susan_edges(unsigned char in [],int r[],unsigned char mid[],unsigned char bpt[], int index)
//TODO ask how to fix these channels in/out/inout as well as what are actually channels
behavior susan_edges(  
  //  in unsigned char input [XSIZE*YSIZE],
		i_uImage_receiver image_in,
 //   inout int r[XSIZE*YSIZE],
  //  inout unsigned char mid[XSIZE*YSIZE],
   // inout unsigned char bpt[516], 
   // inout int index1,
		i_uImage_sender out_image,
		i_uImage_sender to_thin,
		i_intImage_sender Rto_thin
    )

{
 void main (void){
    float z;
    int   do_symmetry, i, j, m, n, a, b, x, y, w;
    unsigned char  c,*p,*cp;
    unsigned char *bp;
	unsigned char input[XSIZE*YSIZE];
	int r[XSIZE*YSIZE];
    unsigned char mid[XSIZE*YSIZE];
    unsigned char bpt[516]; 
    int index1 = 258;
		struct image midStruct;
		struct Rimage rStruct;
		struct image inputStruct;
		struct image input_im;
//------ from setup brightness
    int   k;
    float temp; 
		int index_bright = 258;
//initializations
    for(k=-256;k<257;k++)
    {
          temp=((float)k)/((float)20);
          temp=temp*temp;
       //   if (form==6) always 6
          temp=temp*temp*temp;
          temp=100.0*exp(-temp);
          bpt[k+index_bright]= (unsigned char)temp;
    }

    bp = bpt+index1;
		memset (r,0,XSIZE * YSIZE * sizeof(int));
   	for(i = 0; i<XSIZE*YSIZE; i++){
			mid[i] =100;
		}    
//----------- finish initializations

		image_in.receive(&input_im);	
		input = input_im.imagePack;
	
      for (i=3;i<YSIZE-3;i++)
        for (j=3;j<XSIZE-3;j++)
        {
          n=100;
          p=input + (i-3)*XSIZE + j - 1;
          cp=bp + input[i*XSIZE+j];
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-3; 
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-5;
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-6;
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=2;
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-6;
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-5;
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
          p+=XSIZE-3;
    
          n+=*(cp-*p++);
          n+=*(cp-*p++);
          n+=*(cp-*p);
    
          if (n<=MAXEDGES)
            r[i*XSIZE+j] = MAXEDGES - n;
        }
    
      for (i=4;i<YSIZE-4;i++)
        for (j=4;j<XSIZE-4;j++)
        {
          if (r[i*XSIZE+j]>0)
          {
            m=r[i*XSIZE+j];
            n=MAXEDGES - m;
            cp=bp + input[i*XSIZE+j];
    
            if (n>600)
            {
              p=input + (i-3)*XSIZE + j - 1;
              x=0;y=0;
    
              c=*(cp-*p++);x-=c;y-=3*c;
              c=*(cp-*p++);y-=3*c;
              c=*(cp-*p);x+=c;y-=3*c;
              p+=XSIZE-3; 
        
              c=*(cp-*p++);x-=2*c;y-=2*c;
              c=*(cp-*p++);x-=c;y-=2*c;
              c=*(cp-*p++);y-=2*c;
              c=*(cp-*p++);x+=c;y-=2*c;
              c=*(cp-*p);x+=2*c;y-=2*c;
              p+=XSIZE-5;
        
              c=*(cp-*p++);x-=3*c;y-=c;
              c=*(cp-*p++);x-=2*c;y-=c;
              c=*(cp-*p++);x-=c;y-=c;
              c=*(cp-*p++);y-=c;
              c=*(cp-*p++);x+=c;y-=c;
              c=*(cp-*p++);x+=2*c;y-=c;
              c=*(cp-*p);x+=3*c;y-=c;
              p+=XSIZE-6;
    
              c=*(cp-*p++);x-=3*c;
              c=*(cp-*p++);x-=2*c;
              c=*(cp-*p);x-=c;
              p+=2;
              c=*(cp-*p++);x+=c;
              c=*(cp-*p++);x+=2*c;
              c=*(cp-*p);x+=3*c;
              p+=XSIZE-6;
        
              c=*(cp-*p++);x-=3*c;y+=c;
              c=*(cp-*p++);x-=2*c;y+=c;
              c=*(cp-*p++);x-=c;y+=c;
              c=*(cp-*p++);y+=c;
              c=*(cp-*p++);x+=c;y+=c;
              c=*(cp-*p++);x+=2*c;y+=c;
              c=*(cp-*p);x+=3*c;y+=c;
              p+=XSIZE-5;
    
              c=*(cp-*p++);x-=2*c;y+=2*c;
              c=*(cp-*p++);x-=c;y+=2*c;
              c=*(cp-*p++);y+=2*c;
              c=*(cp-*p++);x+=c;y+=2*c;
              c=*(cp-*p);x+=2*c;y+=2*c;
              p+=XSIZE-3;
    
              c=*(cp-*p++);x-=c;y+=3*c;
              c=*(cp-*p++);y+=3*c;
              c=*(cp-*p);x+=c;y+=3*c;
    
              z = sqrt((float)((x*x) + (y*y)));
              if (z > (0.9*(float)n)) /* 0.5 */
    	  {
                do_symmetry=0;
                if (x==0)
                  z=1000000.0;
                else
                  z=((float)y) / ((float)x);
                if (z < 0) { z=-z; w=-1; }
                else w=1;
                if (z < 0.5) { /* vert_edge */ a=0; b=1; }
                else { if (z > 2.0) { /* hor_edge */ a=1; b=0; }
                else { /* diag_edge */ if (w>0) { a=1; b=1; }
                                       else { a=-1; b=1; }}}
                if ( (m > r[(i+a)*XSIZE+j+b]) && (m >= r[(i-a)*XSIZE+j-b]) &&
                     (m > r[(i+(2*a))*XSIZE+j+(2*b)]) && (m >= r[(i-(2*a))*XSIZE+j-(2*b)]) )
                  mid[i*XSIZE+j] = 1;
              }
              else
                do_symmetry=1;
            }
            else 
              do_symmetry=1;
    
            if (do_symmetry==1)
    	{ 
              p=input + (i-3)*XSIZE + j - 1;
              x=0; y=0; w=0;
    
              /*   |      \
                   y  -x-  w
                   |        \   */
    
              c=*(cp-*p++);x+=c;y+=9*c;w+=3*c;
              c=*(cp-*p++);y+=9*c;
              c=*(cp-*p);x+=c;y+=9*c;w-=3*c;
              p+=XSIZE-3; 
      
              c=*(cp-*p++);x+=4*c;y+=4*c;w+=4*c;
              c=*(cp-*p++);x+=c;y+=4*c;w+=2*c;
              c=*(cp-*p++);y+=4*c;
              c=*(cp-*p++);x+=c;y+=4*c;w-=2*c;
              c=*(cp-*p);x+=4*c;y+=4*c;w-=4*c;
              p+=XSIZE-5;
        
              c=*(cp-*p++);x+=9*c;y+=c;w+=3*c;
              c=*(cp-*p++);x+=4*c;y+=c;w+=2*c;
              c=*(cp-*p++);x+=c;y+=c;w+=c;
              c=*(cp-*p++);y+=c;
              c=*(cp-*p++);x+=c;y+=c;w-=c;
              c=*(cp-*p++);x+=4*c;y+=c;w-=2*c;
              c=*(cp-*p);x+=9*c;y+=c;w-=3*c;
              p+=XSIZE-6;
    
              c=*(cp-*p++);x+=9*c;
              c=*(cp-*p++);x+=4*c;
              c=*(cp-*p);x+=c;
              p+=2;
              c=*(cp-*p++);x+=c;
              c=*(cp-*p++);x+=4*c;
              c=*(cp-*p);x+=9*c;
              p+=XSIZE-6;
        
              c=*(cp-*p++);x+=9*c;y+=c;w-=3*c;
              c=*(cp-*p++);x+=4*c;y+=c;w-=2*c;
              c=*(cp-*p++);x+=c;y+=c;w-=c;
              c=*(cp-*p++);y+=c;
              c=*(cp-*p++);x+=c;y+=c;w+=c;
              c=*(cp-*p++);x+=4*c;y+=c;w+=2*c;
              c=*(cp-*p);x+=9*c;y+=c;w+=3*c;
              p+=XSIZE-5;
     
              c=*(cp-*p++);x+=4*c;y+=4*c;w-=4*c;
              c=*(cp-*p++);x+=c;y+=4*c;w-=2*c;
              c=*(cp-*p++);y+=4*c;
              c=*(cp-*p++);x+=c;y+=4*c;w+=2*c;
              c=*(cp-*p);x+=4*c;y+=4*c;w+=4*c;
              p+=XSIZE-3;
    
              c=*(cp-*p++);x+=c;y+=9*c;w-=3*c;
              c=*(cp-*p++);y+=9*c;
              c=*(cp-*p);x+=c;y+=9*c;w+=3*c;
    
              if (y==0)
                z = 1000000.0;
              else
                z = ((float)x) / ((float)y);
              if (z < 0.5) { /* vertical */ a=0; b=1; }
              else { if (z > 2.0) { /* horizontal */ a=1; b=0; }
              else { /* diagonal */ if (w>0) { a=-1; b=1; }
                                    else { a=1; b=1; }}}
              if ( (m > r[(i+a)*XSIZE+j+b]) && (m >= r[(i-a)*XSIZE+j-b]) &&
                   (m > r[(i+(2*a))*XSIZE+j+(2*b)]) && (m >= r[(i-(2*a))*XSIZE+j-(2*b)]) )
                mid[i*XSIZE+j] = 2;	
            }
          }
        }
		midStruct.imagePack = mid;
		rStruct.imagePack = r;
		to_thin.send(midStruct);
		Rto_thin.send(rStruct);
		inputStruct.imagePack = input;
		out_image.send(inputStruct);		
 }
};

behavior susan_edges_fsm(  
  //  in unsigned char input [XSIZE*YSIZE],
		i_uImage_receiver inq,
 //   inout int r[XSIZE*YSIZE],
  //  inout unsigned char mid[XSIZE*YSIZE],
   // inout unsigned char bpt[516], 
   // inout int index1,
		i_uImage_sender SEdge_to_EdgeDraw,
		i_uImage_sender SEdge_to_Sthin,
		i_intImage_sender SEdge_toSthinR
    )
{
	susan_edges sus_edge(inq,SEdge_to_EdgeDraw, SEdge_to_Sthin,SEdge_toSthinR );
	void main(){
		fsm{
			sus_edge:goto sus_edge;
		}
	}
};

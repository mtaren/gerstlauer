/****************************************************************************
*  Title: Edge_Draw.sc
*  Author: Lucas Holt
*  Date: 2/3/2014
*  Description: Specification model for even parity checker
****************************************************************************/
#define XSIZE 76
#define YSIZE 95
#define INDEX 3610

import "c_queue";
import "imageType";
//import "edge_draw_thread";
import "edge_draw_thread_bott";
import "edge_draw_thread_top";

behavior EdgeDrawTop(
                  inout unsigned char mid[XSIZE*YSIZE],
                  inout unsigned char input[XSIZE*YSIZE],
                  in int number_of_blocks,
                  //in int start_pos,
                  out unsigned char output[XSIZE*YSIZE]
){
EdgeDrawThreadTop EDTTOP1(  mid, input,  2, 0, output);
EdgeDrawThreadTop EDTTOP2(  mid, input,  2, INDEX, output);
    void main (void){
        par{
            EDTTOP1;
            EDTTOP2;
        }
    }

};

behavior EdgeDrawBott(
                  inout unsigned char mid[XSIZE*YSIZE],
                  inout unsigned char input[XSIZE*YSIZE],
                  in int number_of_blocks,
                  //in int start_pos,
                  out unsigned char output[XSIZE*YSIZE]
){
EdgeDrawThreadBott EDTBOTT1(  mid, input,  2, 0, output);
EdgeDrawThreadBott EDTBOTT2(  mid, input,  2, INDEX, output);
    void main (void){
        par{
            EDTBOTT1;
            EDTBOTT2;
        }
    }

};

behavior EdgeDraw(
                    unsigned char mid[XSIZE*YSIZE],
                    unsigned char input[XSIZE*YSIZE],
                    unsigned char output[XSIZE*YSIZE]
                  )
{
 //int number_of_blocks = 1;
 //int start_pos = 0;
//EdgeDrawThread EDT(  outq, in_data_queue, in_from_SThin,  number_of_blocks, start_pos);

//EdgeDrawThread EDT(  mid, input,  2, 0, output);
//EdgeDrawThread EDT2(  mid, input,  2, INDEX, output);

// EDTTOP1
// EDTTOP2
// EDTBOTT1
// EDTBOTT2
//
// EDTMASTERTOP
EdgeDrawTop EDTMASTERTOP(  mid, input,  2, output);
EdgeDrawBott EDTMASTERBOTT(  mid, input,  2, output);
// EDTMASTERBOTT`
	void main(void) {
            EDTMASTERTOP;
            EDTMASTERBOTT;
   }
};



behavior  send_ed(
             i_uImage_sender outq,
             in unsigned char outimage[XSIZE*YSIZE]
             )
{
	void main(void){
		struct image out_im;
		out_im.imagePack = outimage;
		outq.send(out_im);
	}
};

behavior  receive(
			i_uImage_receiver in_data_queue, //from edgeDraw
			i_uImage_receiver in_from_SThin,
            out unsigned char midpack[XSIZE*YSIZE],
            out unsigned char inputpack[XSIZE*YSIZE]
             )
{
    struct image mid; 
    struct image input; 
	void main(void){
		    in_from_SThin.receive(&mid);
		    in_data_queue.receive(&input); //Recieves Image from edgedraw
            midpack = mid.imagePack;
            inputpack = input.imagePack;
	}
};



behavior EdgeDraw_fsm(
                  i_uImage_sender outq,
				  i_uImage_receiver in_data_queue, //from edgeDraw
				  i_uImage_receiver in_from_SThin
                  )
{
    unsigned char midpack [XSIZE*YSIZE];
    unsigned char inputpack[XSIZE*YSIZE];
    unsigned char outimage[XSIZE*YSIZE];

	//EdgeDraw edge_draw(mid.imagePack,  input.imagePack, outimage);
    receive rx (in_data_queue, in_from_SThin, midpack, inputpack);
	EdgeDraw edge_draw(midpack,  inputpack, outimage);
    send_ed sender(outq, outimage);

	void main(void){
		fsm{
           rx: goto edge_draw;
           edge_draw: goto sender;
           sender: goto rx;
		}
	}
};


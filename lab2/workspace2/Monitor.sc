#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sim.sh>
import "c_queue";
import "imageType";
#define XSIZE 76
#define YSIZE 95
#define NUMPICS 5
#define  exit_error(IFB,IFC) { fprintf(stderr,IFB,IFC); exit(0); }

//import "put_Image";
// get unsigned interger from stdin
//TODO Final image goes to a c_queue

void put_image(char *filename,unsigned char in_image[XSIZE*YSIZE]);


behavior Monitor( i_uImage_receiver outq )
{	
	int d = 1;
    unsigned char final_image[XSIZE*YSIZE];
	struct image f_image;
//	put_Image putd(d);
	void main(void) {
            //TODO part c loop over pieces of the image for now it is one image
			//while(1){
				//wait(done);
				int i;
				sim_time_string	buf; 
				for(i = 0; i < NUMPICS; i++){
					outq.receive(&f_image);
					final_image = f_image.imagePack;
					put_image("output_edge.pgm", final_image);
					printf("Time%4s: Done Processiong storing data\n", time2str(buf, now()));
				}

			//}
                exit(0);
		}
};

void put_image(char *filename,unsigned char in_image[XSIZE*YSIZE])
{
FILE  *fd;

#ifdef FOPENB
  if ((fd=fopen(filename,"wb")) == NULL) 
#else
  if ((fd=fopen(filename,"w")) == NULL) 
#endif
    exit_error("Can't output image%s.\n",filename);

  fprintf(fd,"P5\n");
  fprintf(fd,"%d %d\n",XSIZE,YSIZE);
  fprintf(fd,"255\n");
  
  if (fwrite(in_image,XSIZE*YSIZE,1,fd) != 1)
    exit_error("Can't write image %s.\n",filename);

  fclose(fd);
}


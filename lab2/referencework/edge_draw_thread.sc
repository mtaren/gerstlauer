/****************************************************************************
*  Title: Edge_Draw.sc
*  Author: Lucas Holt
*  Date: 2/3/2014
*  Description: Specification model for even parity checker
****************************************************************************/
#define XSIZE 76
#define YSIZE 95
import "c_queue";
import "imageType";

behavior EdgeDrawThread(//in unsigned char in_data[XSIZE*YSIZE],
//                  inout unsigned char mid[XSIZE*YSIZE],
                  in unsigned char midarr[XSIZE*YSIZE],
                  in unsigned char input[XSIZE*YSIZE],
                  in int number_of_blocks,
                  in int start_pos,
                  out unsigned char output[XSIZE*YSIZE]
                  )
{
	void main(void) {
		int   i;
		unsigned char *inp, *midp, *mid, *in_data;
		//struct image midStruct;
		//struct image in_dataStruct;
		//in_from_SThin.receive(&midStruct);
		mid = midarr + start_pos; 
		//in_data_queue.receive(&in_dataStruct); //Recieves Image from edgedraw
		in_data = input+start_pos;
		

		//mark 3x3 white block around each edge point 
		midp=mid;
		for (i=0; i<XSIZE*YSIZE/number_of_blocks; i++)
		{
		  if (*midp<8) 
		  {
			inp = in_data + (midp - mid) - XSIZE - 1;
			*inp++=255; *inp++=255; *inp=255; inp+=XSIZE-2;
			*inp++=255; *inp++;     *inp=255; inp+=XSIZE-2;
			*inp++=255; *inp++=255; *inp=255;
		  }
		  midp++;
		}


		// now mark 1 black pixel at each edge point 
		midp=mid;
		for (i=0; i<XSIZE*YSIZE/number_of_blocks; i++)
		{
			if (*midp<8) 
			*(in_data + (midp - mid)) = 0;
			midp++;
		}

    //outq.send(in_data, XSIZE*YSIZE);
        for (i = 0 ; i< XSIZE*YSIZE/number_of_blocks; i++){
          output [start_pos+i] = input[start_pos+i];
        }


	}
};


#include "susan.sh"

import "i_receiver";
import "i_send";

behavior Monitor(i_receiver img, i_send finish, in sim_time start_time)
{

    void main (void)
    {
        FILE  *fd;
        uchar  image_buffer[IMAGE_SIZE];
        char filename[100];
        int n;
        
        for(n = 0; n < NR_IMGS; n++) 
        {
            sprintf(filename, "out.pgm",  n);

#ifdef FOPENB
            if ((fd=fopen(filename,"wb")) == NULL) 
#else
            if ((fd=fopen(filename,"w")) == NULL) 
#endif
                exit_error("Can't output image%s.\n",filename);

            fprintf(fd,"P5\n");
            fprintf(fd,"%d %d\n", X_SIZE, Y_SIZE);
            fprintf(fd,"255\n");

            img.receive(image_buffer, IMAGE_SIZE*sizeof(char));
    
            if (fwrite(image_buffer,X_SIZE*Y_SIZE,1,fd) != 1)
                exit_error("Can't write image %s.\n",filename);

            fclose(fd);
            
            printf ("Total process time: %llu us\n", (now())/(1 MICRO_SEC));
           // finish.send();
        }     
        exit(0);
    }


};

